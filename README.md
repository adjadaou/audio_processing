# Audio_Processing



## Objectif du notebook
Ce notebook a pour objectif de réaliser un nettoyage des données d'enregistrement d'animaux afin de pouvoir les utiliser pour l'apprentissage d'un modèle de reconnaissance d'espèces.

## Remarque
S'il y a des ralentis lors de l'execution du notebook, il faut penser à nettoyer les sorties de certaines cellules (surtout celles avec un lecteur audio).